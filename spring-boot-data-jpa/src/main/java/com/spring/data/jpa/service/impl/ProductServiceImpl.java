package com.spring.data.jpa.service.impl;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.data.jpa.dao.ProductRepository;
import com.spring.data.jpa.dto.ProductDto;
import com.spring.data.jpa.entity.Product;
import com.spring.data.jpa.mapper.impl.ProductMapper;
import com.spring.data.jpa.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductMapper productMapper;

	@Override
	public ProductDto newProducts(ProductDto productDto) {
		
		Product product = productMapper.mapProductInfo(productDto);
		Product savedProduct = productRepository.save(product);
		ModelMapper mapper = new ModelMapper();
		ProductDto productInfo = mapper.map(savedProduct, ProductDto.class);
		return productInfo;
	}

	

	@Override
	public ProductDto getProducts(Integer id) {
		
		Optional<Product> products = productRepository.findById(id);
		Product product = products.get();
		ModelMapper mapper = new ModelMapper();
		ProductDto productInfo = mapper.map(product, ProductDto.class);
		return productInfo;
	}
}
