package com.spring.data.jpa.mapper.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.spring.data.jpa.dto.ProductDto;
import com.spring.data.jpa.dto.SkuInfoDto;
import com.spring.data.jpa.entity.Brand;
import com.spring.data.jpa.entity.PriceInfo;
import com.spring.data.jpa.entity.Product;
import com.spring.data.jpa.entity.SkuInfo;

@Component
public class ProductMapper {
	
	
	public Product mapProductInfo(ProductDto productDto) {
		Product product = new Product();
		product.setProductName(productDto.getProductName());
		product.setProductDescription(productDto.getProductDescription());
		product.setSellerName(productDto.getSellerName());
		product.setCreatedBy(productDto.getCreatedBy());
		product.setCreatedTs((Date) productDto.getCreatedTs());
		product.setLastUpdatedBy(productDto.getLastUpdatedBy());
		product.setLastUpdatedTs((Date) productDto.getLastUpdatedTs());
		product.setBrand(getBrand(productDto, product));
		product.setSkuInfo(getSkuInfo(productDto, product));
		return product;
		
	}

	private List<SkuInfo> getSkuInfo(ProductDto productDto, Product product) {
		List<SkuInfo> skuList = new ArrayList<>();
		List<SkuInfoDto> skuInfoList = productDto.getSkuInfo();
		for(SkuInfoDto skuInfoDto: skuInfoList) {
			SkuInfo skuInfo = new SkuInfo();
			skuInfo.setSkuName(skuInfoDto.getSkuName());
			skuInfo.setSkuDescription(skuInfoDto.getSkuDescription());
			skuInfo.setImageURL(skuInfoDto.getImageURL());
			skuInfo.setQuantity(skuInfoDto.getQuantity());
			skuInfo.setCreatedBy(skuInfoDto.getCreatedBy());
			skuInfo.setCreatedTs((Date) skuInfoDto.getCreatedTs());
			skuInfo.setLastUpdatedBy(skuInfoDto.getLastUpdatedBy());
			skuInfo.setLastUpdatedTs((Date) skuInfoDto.getLastUpdatedTs());
			skuInfo.setPrice(getPriceInfo(skuInfoDto, skuInfo));
			skuInfo.setProduct(product);
			skuList.add(skuInfo);
		}
		return skuList;
	}

	private PriceInfo getPriceInfo(SkuInfoDto skuInfoDto, SkuInfo skuInfo) {
		PriceInfo priceInfo = new PriceInfo();
		priceInfo.setListPrice(skuInfoDto.getPriceInfo().getListPrice());
		priceInfo.setSalePrice(skuInfoDto.getPriceInfo().getSalePrice());
		priceInfo.setSku(skuInfo);
		return priceInfo;
	}

	private Brand getBrand(ProductDto productDto, Product product) {
		Brand brand = new Brand();
		brand.setBrandName(productDto.getBrand().getBrandName());
		brand.setProduct(product);
		return brand;
	}

}
