package com.spring.data.jpa.service;

import org.springframework.stereotype.Service;

import com.spring.data.jpa.dto.ProductDto;

@Service
public interface ProductService {

	ProductDto newProducts(ProductDto productDto);

	ProductDto getProducts(Integer id);

}
