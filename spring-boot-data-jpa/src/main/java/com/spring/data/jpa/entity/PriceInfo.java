package com.spring.data.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PriceInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer priceId;

	private Double listPrice;

	private Double salePrice;

	@OneToOne
	@MapsId
	@JoinColumn(name = "sku_id")
	private SkuInfo sku;
}
