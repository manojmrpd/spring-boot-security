package com.spring.data.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.spring.data.jpa.dto.ProductDto;
import com.spring.data.jpa.service.ProductService;

@RestController
@RequestMapping("/v1")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	
	@PostMapping("/products")
	public ResponseEntity<ProductDto> newProducts(@RequestBody ProductDto productDto) {
		ProductDto product = productService.newProducts(productDto);
		return new ResponseEntity<ProductDto>(product, HttpStatus.CREATED);
	}
	
	
	@PostMapping("/products/{id}")
	public ResponseEntity<ProductDto> getProductDetails(@PathVariable Integer id){
		ProductDto product = productService.getProducts(id);
		return new ResponseEntity<ProductDto>(product, HttpStatus.OK);
	}

}
