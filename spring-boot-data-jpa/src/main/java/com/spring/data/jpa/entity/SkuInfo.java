package com.spring.data.jpa.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SkuInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer skuId;

	private String skuName;

	private String skuDescription;

	private Integer quantity;
	
	private String imageURL;
	
	 @OneToOne(mappedBy = "sku", cascade = CascadeType.ALL)
	 @PrimaryKeyJoinColumn
	private PriceInfo price;

	private Date createdTs;

	private String createdBy;

	private Date lastUpdatedTs;

	private String lastUpdatedBy;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id")
    private Product product;

}
