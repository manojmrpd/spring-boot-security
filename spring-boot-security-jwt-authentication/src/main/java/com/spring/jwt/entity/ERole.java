package com.spring.jwt.entity;

public enum ERole {
	user,
    moderator,
    admin
}
